# Prometheus & Grafana



## Prometheus
- [Документация Prometheus](https://prometheus.io/)
- [Руководство по Prometheus](https://habr.com/ru/company/southbridge/blog/455290/)
- [Введение в мониторинг серверов](https://habr.com/ru/post/652185/)
- [Мониторинг сервисов с Prometheus](https://habr.com/ru/company/selectel/blog/275803/)
- Видео: [How Prometheus Monitoring works](https://www.youtube.com/watch?v=h4Sl21AKiDg)

## Grafana
- [Документация Grafana](https://grafana.com/grafana/)
- [Вкратце о Grafana](https://habr.com/ru/company/southbridge/blog/431122/)


## Loki
- [Установка и использование Grafana Loki на Linux](https://www.dmosk.ru/instruktions.php?object=grafana-loki)
- [Loki и Grafana](https://habr.com/ru/company/badoo/blog/507718/)
- [Loki - сбор логов в Kubernetes](https://habr.com/ru/company/otus/blog/487118/)
- [Пошаговое руководство](https://infoit.com.ua/linux/kak-pereslat-logi-v-grafana-loki-s-pomoshhyu-promtail/)
